#include <FastLED.h>
#include <SPI.h>
#include <Ethernet.h>

uint8_t aStaticCurrentDemo = 0;

//holy grail
#define A_NUMBER_OF_DEMOS 11

#define LED_PIN     6
#define NUM_LEDS    216
#define BRIGHTNESS  200
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define UPDATES_PER_SECOND 1000 

CRGB leds[NUM_LEDS];
CRGBPalette16 currentPalette;
TBlendType    currentBlending;
extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 177);
EthernetServer server(80);

void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
    uint8_t brightness = 255;
    
    for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
}

void ChangePalettePeriodically(uint8_t demoNumber)
{
  switch(demoNumber)
  {
    case 0:
    {
      currentPalette = RainbowColors_p;
      currentBlending = LINEARBLEND;
      break;
    }
    case 1:
    {
      currentPalette = RainbowStripeColors_p;
      currentBlending = NOBLEND;
      break;
    }
    case 2:
    {
      currentPalette = RainbowStripeColors_p;
      currentBlending = LINEARBLEND;
      break;
    }
    case 3:
    {
      SetupPurpleAndGreenPalette();
      currentBlending = LINEARBLEND;
      break;
    }
    case 4:
    {
      SetupTotallyRandomPalette();
      currentBlending = LINEARBLEND;
      break;
    }
    case 5:
    {
      SetupBlackAndWhiteStripedPalette();
      currentBlending = NOBLEND;
      break;
    }
    case 6:
    {
      SetupBlackAndWhiteStripedPalette();
      currentBlending = LINEARBLEND;
      break;
    }
    case 7:
    {
      currentPalette = CloudColors_p;
      currentBlending = LINEARBLEND;
      break;
    }
    case 8:
    {
      currentPalette = PartyColors_p;
      currentBlending = LINEARBLEND;
      break;
    }
    case 9:
    {
      currentPalette = myRedWhiteBluePalette_p;
      currentBlending = NOBLEND;
      break;
    }
    case 10:
    {
      currentPalette = myRedWhiteBluePalette_p;
      currentBlending = LINEARBLEND;
      break;
    }
    default:
    {
      currentPalette = RainbowColors_p;
      currentBlending = LINEARBLEND;
      break;
    }
  }
}

void SetupTotallyRandomPalette()
{
    for( int i = 0; i < 16; i++) {
        currentPalette[i] = CHSV( random8(), 255, random8());
    }
}

void SetupBlackAndWhiteStripedPalette()
{
    fill_solid( currentPalette, 16, CRGB::Black);
    currentPalette[0] = CRGB::White;
    currentPalette[4] = CRGB::White;
    currentPalette[8] = CRGB::White;
    currentPalette[12] = CRGB::White;
    
}

void SetupPurpleAndGreenPalette()
{
    CRGB purple = CHSV( HUE_PURPLE, 255, 255);
    CRGB green  = CHSV( HUE_GREEN, 255, 255);
    CRGB black  = CRGB::Black;
    
    currentPalette = CRGBPalette16(
                                   green,  green,  black,  black,
                                   purple, purple, black,  black,
                                   green,  green,  black,  black,
                                   purple, purple, black,  black );
}

const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Red,
    CRGB::Gray,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Blue,
    CRGB::Black,
    CRGB::Black
};

void setup() {
  delay( 3000 );
  Serial.begin(9600);
  Serial.println("STARTING::SERIAL");
  while (!Serial) {
    ;
  }
  Serial.println("STARTED::SERIAL");
  
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(  BRIGHTNESS );
  
  currentPalette = RainbowColors_p;
  currentBlending = LINEARBLEND;

  if(!Ethernet.begin(mac))
  {
    Serial.println("failed with dhcp");

    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
        Serial.println("Ethernet shield was not found. looping forever");
        /*while(1)
        {
           ;
        }*/
    }

    else if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Link status: Off");
    }
  }

  else
  {
    Ethernet.begin(mac, ip);
    Serial.println("success w static");
  }

  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}
//String tmp = "";
String responseString = "";

void loop() {
  EthernetClient client = server.available();
  if (client) {
    //Serial.println("new client");
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
         if (responseString.length() < 14) {
          responseString += c;
         }
        if (c == '\n' && currentLineIsBlank) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          //client.println("Refresh: 5");
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          {
            for(uint8_t i = 0; i < A_NUMBER_OF_DEMOS; i++)
            {
              //String tmp;
  
              client.print("<a href=\"/?button");
              client.print(i);
              client.print("\">Turn On LED");
              client.print(i);
              client.print("</a><br>");
              client.println("");
            }
          }
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        } else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    //should work even on dial up(56kb/s)
    delay(10);
    client.stop();
    Serial.println(responseString);
    responseString.replace("GET", "");
    responseString.replace(" ", "");
    Serial.println(responseString);

    String tmp = "";
    for(uint8_t i = 0; i < A_NUMBER_OF_DEMOS; i++)
    {
      tmp = "/?button";
      tmp += i;
      if(responseString == tmp)
      {
        Serial.println(i);
        aStaticCurrentDemo = i;
        break;
      }
    }
    responseString = "";
    //Serial.println("client disconnected");
  }
  static uint8_t startIndex = 0;
  startIndex = startIndex + 1;
  ChangePalettePeriodically(aStaticCurrentDemo);
  FillLEDsFromPaletteColors(startIndex);
  FastLED.show();
  FastLED.delay(1000 / UPDATES_PER_SECOND);

}

