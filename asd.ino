#include <Ethernet.h>
#include <MFRC522.h>
#include <SPI.h>

boolean _new[16];
boolean _old[16];

#define ETHERNET_PIN 10
#define RFID_PIN 8
#define SS_PIN 8
#define RST_PIN 9
#define SIG_pin A0
#define s0 2
#define s1 3
#define s2 5
#define s3 6
#define A_CLIENT_SUBNET 1
#define A_CLIENT_IP 46
#define A_SERVER_SUBNET 1
#define A_SERVER_IP 2
#define A_GATEWAY_IP 1
#define A_LOCKPIN 7

MFRC522 mfrc522(SS_PIN, RST_PIN);

IPAddress aServerIP(192, 168, A_SERVER_SUBNET, A_SERVER_IP);

uint8_t aStaticMAC[6] = {
	0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress aStaticIP(192, 168, A_CLIENT_SUBNET, A_CLIENT_IP);
IPAddress aStaticDNS(192, 168, A_CLIENT_SUBNET, A_GATEWAY_IP);

EthernetClient aStaticEthernetClient;

void aStaticEnableEthernet()
{
	pinMode(ETHERNET_PIN, OUTPUT);
	digitalWrite(ETHERNET_PIN, LOW);
}
void aStaticDisableEthernet()
{
	pinMode(ETHERNET_PIN, OUTPUT);
	digitalWrite(ETHERNET_PIN, HIGH);
}

void aStaticEnableRFID()
{
	pinMode(RFID_PIN, OUTPUT);
	digitalWrite(RFID_PIN, LOW);
}

void aStaticDisableRFID()
{
	pinMode(RFID_PIN, OUTPUT);
	digitalWrite(RFID_PIN, HIGH);
}

int aCompare(boolean *a, boolean *b)
{
	int i = 0;
	while(i < 16 && _new[i] == _old[i])
	{
		i++;
	}

	if(i < 16)
	{
		return i;
	}

	return -1;
}

void aRefresh(boolean *a)
{
	for(int i = 0; i < 16; i++)
	{
		a[i] = aReadMux(i);
	}
}

boolean aReadMux(int channel)
{
	int controlPin[] = {s0, s1, s2, s3};
	int muxChannel[16][4] = {
		{0, 0, 0, 0},
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{1, 1, 0, 0},
		{0, 0, 1, 0},
		{1, 0, 1, 0},
		{0, 1, 1, 0},
		{1, 1, 1, 0},
		{0, 0, 0, 1},
		{1, 0, 0, 1},
		{0, 1, 0, 1},
		{1, 1, 0, 1},
		{0, 0, 1, 1},
		{1, 0, 1, 1},
		{0, 1, 1, 1},
		{1, 1, 1, 1}
	};

	for(int i = 0; i < 4; i ++)
	{
		digitalWrite(controlPin[i], muxChannel[channel][i]);
	}

	int val = analogRead(SIG_pin);

	if(val < 512)
	{
		return false;
	}

	else
	{
		return true;
	}
}

void setup()
{
	Serial.begin(9600);
	Serial.println("INFO::SERIAL::WAITING_FOR_CONNECTION");
	while(!Serial)
	{
		;
	}
	pinMode(A_LOCKPIN, OUTPUT);
	digitalWrite(A_LOCKPIN, LOW);
	Serial.println("INFO::SERIAL::CONNECTED");
	Serial.println("");
	SPI.begin();
	aStaticDisableEthernet();
	aStaticEnableRFID();
	mfrc522.PCD_Init();
	Serial.println("INFO::RFID::RFID_INITIALIZED");

	aStaticDisableRFID();

	// disable sd card
	pinMode(4, OUTPUT);
	digitalWrite(4, HIGH);

	aStaticEnableEthernet();

	Serial.println("INFO::ETHERNET::TRYING_WITH_DHCP");
	if(Ethernet.begin(aStaticMAC) == 0)
	{
		Serial.println("WARNING::ETHERNET::FAILED_WITH_DHCP");
		if(Ethernet.hardwareStatus() == EthernetNoHardware)
		{
			Serial.println("ERROR::ETHERNET::SHIELD_NOT_FOUND");
		}
		if(Ethernet.linkStatus() == LinkOFF)
		{
			Serial.println("ERROR::ETHERNET::CABLE_NOT_CONNECTED");
		}

		while(true)
		{
			delay(1); // do nothing
		}
	}

	else
	{
		Serial.print("    INFO::ETHERNET::DHCP_ASSIGNED_IP ");
	}

	Ethernet.begin(aStaticMAC, aStaticIP, aStaticDNS);
	Serial.println("INFO::ETHERNET::ETHERNET_INITIALIZED");
	Serial.println(Ethernet.localIP());
	Serial.println("");
	pinMode(s0, OUTPUT);
	pinMode(s1, OUTPUT);
	pinMode(s2, OUTPUT);
	pinMode(s3, OUTPUT);

	digitalWrite(s0, LOW);
	digitalWrite(s1, LOW);
	digitalWrite(s2, LOW);
	digitalWrite(s3, LOW);

	//get inital key state
	aRefresh(_old);
}

void loop()
{
	aStaticEnableRFID();
	aStaticDisableEthernet();

	// Look for new cards
	if(!mfrc522.PICC_IsNewCardPresent())
	{
		return;
	}

	// Select one of the cards
	if(!mfrc522.PICC_ReadCardSerial())
	{
		return;
	}

	String id = "";
	for(byte i = 0; i < mfrc522.uid.size; i++)
	{
		id += mfrc522.uid.uidByte[i];
	}

	Serial.println("        INFO::RFID::GOT_CARD");
	Serial.println(id);
	Serial.println("");

	aStaticDisableRFID();
	aStaticEnableEthernet();

	delay(1000);

	if(aStaticEthernetClient.connect(aServerIP, 80))
	{
		Serial.println("        INFO::SERVER::CONNECTION");
		aStaticEthernetClient.print("GET /Rebuild/en/php/rfid.php?tag="); //Connecting and Sending values to database
		aStaticEthernetClient.println(id);
		String responseStatus = aStaticEthernetClient.readString();
		Serial.print("response status:");
		Serial.println(responseStatus);
		if(responseStatus == "1" && aStaticEthernetClient.connect(aServerIP, 80))
		{
		Serial.println("opening");
		digitalWrite(A_LOCKPIN, HIGH);
		delay(1000);
		digitalWrite(A_LOCKPIN, LOW);

		int changedHolder = 0;
		while((changedHolder = aCompare(_old, _new)) < 0)
		{
			aRefresh(_new);
			delay(100);
		}
		aRefresh(_old);
		int table[] { 0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15};
		int result = table[changedHolder];

		result++;
		if(_new[changedHolder] == 0)
		{
			result = -result;
		}
		aStaticEthernetClient.print("GET /Rebuild/en/includes/borrowrfid.inc.php?key=");
		aStaticEthernetClient.println(result);

		responseStatus = aStaticEthernetClient.readString();
		Serial.println(responseStatus);

		Serial.println(result);
		}

		else
		{
			Serial.println("no unlock for you :c");
		}
		aStaticEthernetClient.stop(); //Closing the connection
	}

	else
	{
		Serial.println("ERROR::SERVER::CONNECTION_TO_SERVER_FAILED");
		Serial.println("        Please check your server setup in this program(IP, and index.php) and on the server side");
	}

	delay(100);
}